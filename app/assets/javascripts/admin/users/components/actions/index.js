import Activate from './activate.vue';
import Approve from './approve.vue';
import Block from './block.vue';
import Deactivate from './deactivate.vue';
import Delete from './delete.vue';
import DeleteWithContributions from './delete_with_contributions.vue';
import Unblock from './unblock.vue';
import Unlock from './unlock.vue';
import Reject from './reject.vue';

export default {
  Activate,
  Approve,
  Block,
  Deactivate,
  Delete,
  DeleteWithContributions,
  Unblock,
  Unlock,
  Reject,
};
